import cv2
import numpy as np
import matplotlib.pyplot as plt

def good_points_from(des1, des2):
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)
    ratio = 0.6
    return len(filter(lambda x: abs(x[0].distance - x[1].distance) < 5, matches))
    # return len(filter(lambda x: x[0].distance < ratio*x[1].distance, matches))




img1 = cv2.imread('/Users/drink/Desktop/drink-icon-1.png')
img2 = cv2.imread('/Users/drink/Desktop/error-image2.png')
img3 = cv2.imread('/Users/drink/Desktop/error-image.png')

sift = cv2.xfeatures2d.SIFT_create()

kp1, des1 = sift.detectAndCompute(img1, None)
kp2, des2 = sift.detectAndCompute(img2, None)
kp3, des3 = sift.detectAndCompute(img3, None)


print good_points_from(des1, des2)
print good_points_from(des1, des3)
print good_points_from(des2, des3)
