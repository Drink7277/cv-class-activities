import cv2
import numpy as np
import ipdb

video_file = '/Users/drink/cv/P6090053.avi'

cap = cv2.VideoCapture(video_file)

total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT));
learn_until_frame = int(total_frame * 0.3);

print(total_frame)
print(learn_until_frame)

if (cap.isOpened()== False):
    print("Error opening video stream or file")

ret, prev = cap.read()
diff = np.zeros(prev.shape)
avrDiff = np.zeros(prev.shape)
avgImage = np.zeros(prev.shape)

while(cap.isOpened()):
    ret, frame = cap.read()
    print(frame.shape)
    # if ret == True:
    #     cv2.imshow('Frame', frame)
    #
    #     if cv2.waitKey(25) & 0xFF == ord('q'):
    #       break

    if cap.get(cv2.CAP_PROP_POS_FRAMES) < learn_until_frame:
        cv2.absdiff(prev, frame, diff)
        cv2.accumulate(diff, avrDiff);
        cv2.accumulate(frame, avgImage);
    else:
        lower = (avgImage - 3*avrDiff) /learn_until_frame
        upper = (avgImage + 3*avrDiff) /learn_until_frame
        for i in xrange(frame.shape[0]):
            for j in xrange(frame.shape[1]):
                ipdb.set_trace()
                frame[i,j,:] = cv2.inRange(frame[i,j,:], lower[i,j,:], upper[i,j,:])

        cv2.imshow('Frame', frame)


    if cv2.waitKey(25) & 0xFF == ord('q'):
      break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
