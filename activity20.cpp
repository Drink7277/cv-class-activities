#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define INPUT "/Users/drink/Desktop/Broadway_tower_edit.jpg"

int main(){

    Mat img =  imread(INPUT, IMREAD_GRAYSCALE);
    Canny(img, img, 200.0, 240.0);
    imshow("Image", img);
    waitKey();

    return 0;
}
