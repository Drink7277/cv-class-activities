import cv2
import numpy as np
import matplotlib.pyplot as plt

img_bgr = cv2.imread('/Users/drink/Desktop/drink-icon-1.png')

img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
dst = cv2.cornerHarris(img_gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
img_bgr[dst>0.01*dst.max()]=[0,0,255]

cv2.imshow('image', img_bgr)
cv2.waitKey(0)
