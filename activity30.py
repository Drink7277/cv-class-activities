import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('/Users/drink/Desktop/dolphinStretch2.jpg', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('/Users/drink/Desktop/drink-icon-1.png', cv2.IMREAD_GRAYSCALE)

mean = np.mean(img)
hist = cv2.calcHist([img],[0],None,[256],[0,256]).ravel()

count = 0
while True:
    index = int(round(mean))
    # tb = np.mean(img[:index])
    # tw = np.mean(img[index:])
    tb = sum(hist[:index] * np.arange(index)) / sum(hist[:index])
    tw = sum(hist[index:] * np.arange(index, 256)) / sum(hist[index:])

    new_mean = (tb + tw)/2
    print(mean, '==', new_mean)
    print(abs(mean - new_mean))
    if abs(mean - new_mean) < 0.5:
        break

    mean = new_mean

ret,thresh = cv2.threshold(img,mean,255,cv2.THRESH_BINARY)

cv2.imshow('image', thresh)
cv2.waitKey(0)

# plt.hist(img.ravel(),256,[0,256]);
# plt.show()
# cv2.imshow('image',img)
# cv2.waitKey(0)
