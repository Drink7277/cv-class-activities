#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main(){

    Mat img = imread("drink-icon.png", IMREAD_COLOR);
    namedWindow("Input");
    imshow("Input", img);

    Mat sobel_x, sobel_y;
    Mat abs_sobel_x, abs_sobel_y;

    Mat img_gray;
    cvtColor(img, img_gray, COLOR_BGR2GRAY);

    // Gradient x
    Sobel(img_gray, sobel_x, CV_16S, 1,0, 3);
    convertScaleAbs(sobel_x, abs_sobel_x);
    namedWindow("SobelX");
    imshow("SobelX", abs_sobel_x);

    // Gradient Y
    Sobel(img_gray, sobel_y, CV_16S, 0,1, 3);
    convertScaleAbs(sobel_y, abs_sobel_y);
    namedWindow("SobelY");
    imshow("SobelY", abs_sobel_y);

    Mat grad_magnitude(img.rows, img.cols, CV_8UC1);
    for (int y=0; y < img.rows; y++){
        for (int x=0; x < img.cols; x++){
            Point point(x,y);
            uchar point_x = abs_sobel_x.at<uchar>(point);
            uchar point_y = abs_sobel_y.at<uchar>(point);
            uchar magnitude = sqrt((point_x*point_x + point_y*point_y)/2.0);
            grad_magnitude.at<uchar>(point) = magnitude;
        }
    }

    namedWindow("Gradient Magnitude");
    imshow("Gradient Magnitude", grad_magnitude);

    waitKey(0);
    return 0;
}
