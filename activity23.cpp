#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

uchar lbp(const Mat img, int row, int col){
    uchar output = 0;
    uchar val = img.at<uchar>(row, col);
    uchar p[8];
    bool edge = (col == 0 || row == 0 || col == (img.cols - 1) || row == (img.rows - 1));

    p[0] = (edge)? val : img.at<uchar>(row, col-1);
    p[1] = (edge)? val : img.at<uchar>(row+1, col-1);
    p[2] = (edge)? val : img.at<uchar>(row+1, col);
    p[3] = (edge)? val : img.at<uchar>(row + 1, col + 1);
    p[4] = (edge)? val : img.at<uchar>(row, col + 1);
    p[5] = (edge)? val : img.at<uchar>(row - 1, col + 1);
    p[6] = (edge)? val : img.at<uchar>(row - 1, col);
    p[7] = (edge)? val : img.at<uchar>(row - 1, col - 1);

    for(int i=0; i < 8; i++){
        printf("%d\n", p[i]);
        output |= (p[i] > val) ? (1 << i) : 0;
    }
    return output;
}

int main(){
    Mat img =  (Mat_<uchar>(7,7) << 87, 87, 136, 173, 39, 102, 102,
                                    87, 87, 136, 173, 39, 102, 102,
                                    76, 76, 13, 1, 168, 159, 159,
                                    120, 120, 70, 14, 150, 145, 145,
                                    58, 58, 86, 119, 98, 100, 100,
                                    30, 30, 65, 157, 75, 78, 78,
                                    30, 30, 65, 157, 75, 78, 78
                );
    imshow("Image", img);

    printf("(1,1): %d\n", lbp(img, 1, 1));
    printf("(3,3): %d\n", lbp(img, 3, 3));
    printf("(4,3): %d\n", lbp(img, 4, 3));

    waitKey(0);
}
