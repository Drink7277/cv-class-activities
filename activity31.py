import cv2
import numpy as np
import matplotlib.pyplot as plt
import ipdb

def entropy(hist, t):
    hbn = t
    hwn = 256 - t
    hb = sum([ i/hbn * np.log10(i/hbn + 1) for i in hist[:t]])
    hw = sum([ i/hwn * np.log10(i/hwn + 1) for i in hist[t:]])
    return hb + hw


img = cv2.imread('/Users/drink/Desktop/dolphinStretch2.jpg', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('/Users/drink/Desktop/drink-icon-1.png', cv2.IMREAD_GRAYSCALE)

hist = cv2.calcHist([img],[0],None,[256],[0,256]).ravel()

es = [ entropy(hist, i) for i in xrange(256)]

print es

threshold = np.where(es == np.amax(es))[0][0]

ret,thresh = cv2.threshold(img,threshold,255,cv2.THRESH_BINARY)

# ipdb.set_trace()

cv2.imshow('image', thresh)
cv2.waitKey(0)
