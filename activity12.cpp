#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio.hpp>

using namespace cv;
using namespace std;

int main() {
  VideoCapture cap("P6090053.avi");
  if (!cap.isOpened()) {
    return -1;
  }

  int frame_width = cap.get(CAP_PROP_FRAME_WIDTH);
  int frame_height = cap.get(CAP_PROP_FRAME_HEIGHT);
  VideoWriter video("out.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 25, Size(frame_width, frame_height), true);
  bool startRecord = false;

  while(true) {
    Mat frame;//, edge;
    cap >> frame;

    if (frame.empty())
      break;

    // cv::Canny(frame, edge, 20, 60, 3);
    imshow("Frame", frame );
    // imshow("Edge", edge );

    char c = (char) waitKey(25); // esc
    if (c == 27)
      break;
    else if( c == 's')
      startRecord = true;
    else if( c == 'e')
      startRecord = false;

    if(startRecord)
      video.write(frame);
  }

  cap.release();
  destroyAllWindows();
}
