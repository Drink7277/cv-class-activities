import cv2
import matplotlib.pyplot as plt

imagePath = '/Users/drink/Downloads/39153052391_bc86d7ca42_b.jpg'
cascPath  = '/Users/drink/cv/haarcascade_eye.xml'
faceCascade = cv2.CascadeClassifier(cascPath)
img = cv2.imread(imagePath)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30)
)

for (x,y,w,h) in faces:
    cv2.rectangle(img, (x,y), (x+w, y+h), (0,255,0), 2)

img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img_rgb)
plt.show()
# cv2.waitKey(0)
