#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define INPUT "/Users/drink/cv/drink-icon.png"

const int histSize = 256;
const float range[] = {0, 256};
const float* histRange = {range};

uchar lbp(const Mat img, int row, int col){
    uchar output = 0;
    uchar val = img.at<uchar>(row, col);
    uchar p[8];
    bool edge = (col == 0 || row == 0 || col == (img.cols - 1) || row == (img.rows - 1));

    p[0] = (edge)? val : img.at<uchar>(row, col-1);
    p[1] = (edge)? val : img.at<uchar>(row+1, col-1);
    p[2] = (edge)? val : img.at<uchar>(row+1, col);
    p[3] = (edge)? val : img.at<uchar>(row + 1, col + 1);
    p[4] = (edge)? val : img.at<uchar>(row, col + 1);
    p[5] = (edge)? val : img.at<uchar>(row - 1, col + 1);
    p[6] = (edge)? val : img.at<uchar>(row - 1, col);
    p[7] = (edge)? val : img.at<uchar>(row - 1, col - 1);

    for(int i=0; i < 8; i++){
        output |= (p[i] > val) ? (1 << i) : 0;
    }
    return output;
}

int main(){

    Mat img =  imread(INPUT, IMREAD_GRAYSCALE);
    imshow("Image", img);

    Mat img_lbp = Mat::zeros(img.rows, img.cols, CV_8UC1);
    for(int row = 0 ; row < img.rows; row++){
        for(int col = 0 ; col < img.cols ; col ++){
            img_lbp.at<uchar>(row, col) =lbp(img, row, col);
        }
    }

    Mat img_hist = Mat::zeros(img.rows, img.cols, CV_8UC1);
    Mat hist;
    imshow("img_lbp", img_lbp);
    calcHist(&img_lbp, 1, 0, Mat(), hist, 1, &histSize, &histRange, true, false);

    for(int i = 0; i < img.rows ; i++){
        printf("%d: %d\n", i, (int) hist.at<float>(i,0));
    }

    waitKey(0);
    return 0;
}
