#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main(){

  Mat img(100, 100, CV_8UC3, Scalar(0,0,255));

  circle(img, Point(50,30),  10, Scalar(0,255,0), FILLED);
  rectangle(img, Rect(40, 40, 20, 50), Scalar(255,0,0), FILLED);
  line(img, Point(40,50), Point(20,70), Scalar(255,255,0));
  line(img, Point(60,50), Point(80,70), Scalar(255,255,0));

  imshow("Display", img);
  waitKey(0);

  return 0;
}
