direction = [80, 36, 5, 37, 9, 9, 87, 136, 173]
magnitude = [2,3,4,5,11,17,11,21,23]

h = {}
direction.zip(magnitude).each do |dir, mag|
  if dir % 20 == 0
    h[dir] ||= 0
    h[dir] += mag
  else
    n = (dir / 20)
    new_dir = [n, n+1].map{|i| i * 20}

    h[new_dir.first] ||= 0
    h[new_dir.last]  ||= 0

    h[new_dir.first] += (new_dir.last - dir).abs/20.0 * mag
    h[new_dir.last]  +=  (new_dir.first - dir).abs/20.0 * mag
  end

end

h[0] ||= 0
h[0] += h[180]
h.delete(180)

puts h.sort.to_h
