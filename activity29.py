import cv2
import numpy as np
import matplotlib.pyplot as plt

img_bgr = cv2.imread('/Users/drink/Desktop/drink-icon-1.png')
img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)

mean = np.mean(img)

ret,thresh = cv2.threshold(img,mean,255,cv2.THRESH_BINARY)

cv2.imshow('image', thresh)
cv2.waitKey(0)
