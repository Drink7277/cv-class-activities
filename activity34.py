import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets.samples_generator import make_blobs


# plt.style.use('ggplot')
X, y_true = make_blobs(n_samples=200, centers=5, cluster_std=1.0, random_state=10)
# plt.scatter(X[:,0], X[:,1], s=10)


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
flags = cv2.KMEANS_RANDOM_CENTERS

x = []
y = xrange(2,8)

for n in xrange(2,8):
    compactness, labels, centers = cv2.kmeans(X.astype(np.float32), n,None, criteria, 10, flags)
    x.append(compactness)

plt.plot(y, x)

# plt.scatter(X[:,0], X[:,1], c=labels[:,0], s=10, cmap='viridis')
# plt.scatter(centers[:,0], centers[:,1], c='black', s=50, alpha=0.5)
#
plt.show()
