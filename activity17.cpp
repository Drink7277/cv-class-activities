#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main(){

    Mat img = imread("drink-icon.png", IMREAD_COLOR);
    namedWindow("Input");
    imshow("Input", img);
    Mat sobel_x, sobel_y;
    Mat abs_sobel_x, abs_sobel_y;

    Mat img_gray;
    cvtColor(img, img_gray, COLOR_BGR2GRAY);

    // Gradient x
    Sobel(img_gray, sobel_x, CV_16S, 1,0, 3);
    convertScaleAbs(sobel_x, abs_sobel_x);
    namedWindow("SobelX");
    imshow("SobelX", abs_sobel_x);

    // Gradient Y
    Sobel(img_gray, sobel_y, CV_16S, 0,1, 3);
    convertScaleAbs(sobel_y, abs_sobel_y);
    namedWindow("SobelY");
    imshow("SobelY", abs_sobel_y);

    Mat grad_magnitude(img.rows, img.cols, CV_8UC1);
    Mat direction_img(img.rows, img.cols, CV_8UC3);
    for (int y=0; y < img.rows; y++){
        for (int x=0; x < img.cols; x++){
            Point point(x,y);
            uchar point_x = abs_sobel_x.at<uchar>(point);
            uchar point_y = abs_sobel_y.at<uchar>(point);
            uchar magnitude = sqrt((point_x*point_x + point_y*point_y)/2.0);
            grad_magnitude.at<uchar>(point) = magnitude;

            Vec3b color;
            double deg = atan(point_y/(double) point_x) * 180 / M_PI;
            if (deg < 0) deg += 360;

            if( deg >= 0 && deg < 45) color = Vec3b(0, 0, 255);  // red
            else if ( deg >= 45 && deg < 90) color = Vec3b(0, 255, 0);  // green
            else if ( deg >= 90 && deg < 135) color = Vec3b(255, 0, 0);  // blue
            else if ( deg >= 135 && deg < 180) color = Vec3b(0, 0, 0);  // black
            else if ( deg >= 180 && deg < 225) color = Vec3b(0, 165, 255);  // orange
            else if ( deg >= 225 && deg < 270) color = Vec3b(0, 255, 255);  // yellow
            else if ( deg >= 270 && deg < 315) color = Vec3b(255, 255, 255);  // white
            else if ( deg >= 315 && deg < 360) color = Vec3b(203, 192, 255);  // pink

            direction_img.at<Vec3b>(point) = color;
        }
    }

    namedWindow("Gradient Magnitude");
    imshow("Gradient Magnitude", grad_magnitude);

    namedWindow("Gradient Direction");
    imshow("Gradient Direction", direction_img);

    waitKey(0);
    return 0;
}
