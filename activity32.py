import cv2
import numpy as np
import matplotlib.pyplot as plt
import ipdb

img = cv2.imread('/Users/drink/Desktop/drink-icon-1.png')
# img = cv2.imread('/Users/drink/Desktop/dolphinStretch2.jpg')
mask = np.zeros(img.shape[:2],np.uint8)

rect = (60,45,130,160)
# rect = (180,10,150,90)

# cv2.rectangle(img, rect, (0,0,0),5)

bgdModel = np.zeros((1,65),np.float64)
fgdModel = np.zeros((1,65),np.float64)

cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)

mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
img = img*mask2[:,:,np.newaxis]

img2 = cv2.imread('/Users/drink/Desktop/dolphinStretch2.jpg')
img2[:min(img.shape[0], img2.shape[0]),:img.shape[1],:] += img[:min(img.shape[0], img2.shape[0]),:,:]

# ipdb.set_trace()

cv2.imshow('image', img)
cv2.imshow('image2', img2)
cv2.waitKey(0)
