#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define INPUT "/Users/drink/cv/drink-icon.png"

int main(){

    Mat img =  imread(INPUT, IMREAD_COLOR);
    imshow("Image", img);

    Mat edge;
    Canny(img, edge, 200.0, 240.0);
    imshow("edge", edge);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    findContours(edge, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
    Mat output = Mat::zeros(img.rows, img.cols, CV_8UC3);

    for(int idx= 0; idx >=0; idx = hierarchy[idx][0]){
//        Scalar color((char) rand(),(char) rand(),(char) rand());
//        drawContours(output, contours, idx, color, FILLED, 8, hierarchy);
        printf("idx\n");
    }

    printf("There are %d contours\n", contours.size());
    for (int i = 0; i<contours.size(); i++) {
//        Scalar color((char) rand(),(char) rand(),(char) rand());
        Scalar color(255,255,255);
        drawContours(output, contours, i, color, FILLED, 8, hierarchy);
        printf("%d: %d points\n", i+1, contours[i].size());
    }
    imshow("Output", output);

    waitKey(0);
    return 0;
}
