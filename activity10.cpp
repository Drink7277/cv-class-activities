#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define MODE 1

int main(){

  Mat img = imread("drink-icon.png", IMREAD_COLOR);
  Mat hsv, blur;
  cvtColor(img, hsv, COLOR_BGR2HSV);
  GaussianBlur(img, blur, Size(11, 11),0 ,0);
  // namedWindow("input", WINDOW_AUTOSIZE);
  imshow("Display window", img);
  imshow("hsv", hsv);
  imshow("blur", blur);
  waitKey(0);

  return 0;
}

// #g++ $(pkg-config --cflags --libs /usr/local/Cellar/opencv/4.0.1/lib/pkgconfig/opencv4.pc) -std=c++11  activity10.cpp -o act; ./act
