#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;
#define INPUT "/Users/drink/CV/cards/A.png"
#define NUMBER_OF_CARD 3

const char *LIST_OF_CARDS[NUMBER_OF_CARD] = {
    "/Users/drink/CV/cards/10.png",
    "/Users/drink/CV/cards/A.png",
    "/Users/drink/CV/cards/2.png"
};


const int histSize = 256;
const float range[] = {0, 256};
const float* histRange = {range};

int main(){
    vector<Mat> img_bgr, card_bgr;
    Mat img_hist, card_hist;

    Mat img =  imread(INPUT, IMREAD_COLOR);
    split(img, img_bgr);

    calcHist(&img_bgr[0], 1, 0, Mat(), img_hist, 1, &histSize, &histRange, true, false);
    normalize(img_hist, img_hist, 0, img_hist.rows, NORM_MINMAX, -1, Mat());

    for(int i = 0; i < NUMBER_OF_CARD; i ++){
        Mat card = imread(LIST_OF_CARDS[i], IMREAD_COLOR);
        split(card, card_bgr);

        calcHist(&card_bgr[0], 1, 0, Mat(), card_hist, 1, &histSize, &histRange, true, false);
        normalize(card_hist, card_hist, 0, card_hist.rows, NORM_MINMAX, -1, Mat());

        printf("%d:%f\n",i ,compareHist(img_hist, card_hist, HISTCMP_CORREL));

    }

    return(0);
}
