# from IPython import embed
import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt


images = datasets.fetch_olivetti_faces().images[:10]

plt.figure(figsize=(14, 4))

for index, img in enumerate(images, start=1):
    plt.subplot(4, 5, index)
    plt.imshow(img)

plt.show()
