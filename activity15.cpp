#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

Mat img;

void mouse_callback(int event, int x, int y, int flag, void *param){
  if(event == EVENT_LBUTTONDOWN) {
    Vec3b color = img.at<Vec3b>(Point(x, y));
    cout << "(" << x << ", " << y << ")" << endl;
    cout << "(" << (int) color[2] << ", " << (int) color[1] << ", " << (int) color[0] << ")" << endl;
  }
}

int main(){

  img = imread("drink-icon.png", IMREAD_COLOR);

  cout << "adfads" << endl;
  namedWindow("Mouse");
  setMouseCallback("Mouse", mouse_callback);

  imshow("Mouse", img);
  waitKey();

  return 0;
}
