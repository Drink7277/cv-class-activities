import cv2
import numpy as np

video_file = '/Users/drink/cv/P6090053.avi'

cap = cv2.VideoCapture(video_file)

# Check if camera opened successfully
if (cap.isOpened()== False):
    print("Error opening video stream or file")

ret, frame = cap.read()
prev2 = prev = frame

# Read until video is completed
while(cap.isOpened()):

  prev2 = prev
  prev = frame

  # ret, prev = cap.read()

  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:

    # Display the resulting frame
    cv2.imshow('Frame',cv2.absdiff(prev2, frame))

    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break

  # Break the loop
  else:
    break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
