import cv2
import numpy as np
import matplotlib.pyplot as plt

img_bgr = cv2.imread('/Users/drink/Desktop/drink-icon-1.png')
# img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
# img = cv2.imread('/Users/drink/Desktop/drink-icon-1.png', cv2.IMREAD_GRAYSCALE)
img = img_bgr

# sift = cv2.xfeatures2d.SIFT_create()
# surf = cv2.xfeatures2d.SURF_create()

orb = cv2.ORB_create(nfeatures=1500)

keypoints, descriptors = orb.detectAndCompute(img, None)

img = cv2.drawKeypoints(img, keypoints, None)

cv2.imshow("Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
