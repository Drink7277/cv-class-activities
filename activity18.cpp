#include <stdio.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main(){
    Mat img1 = imread("/Users/drink/Desktop/drink-icon-1.png", IMREAD_COLOR);
    Mat img2 = imread("/Users/drink/Desktop/cloud_error-01-512.png", IMREAD_COLOR);
    Mat img3 = imread("/Users/drink/Desktop/error-image.png", IMREAD_COLOR);

    vector<Mat> img_bgr1, img_bgr2, img_bgr3;
    split(img1, img_bgr1);
    split(img2, img_bgr2);
    split(img3, img_bgr3);

    int histSize = 256;

    float range[] = {0, 256};
    const float* histRange = {range};

    bool uniform = true;
    bool accumulate = false;

    Mat r_hist1, b_hist1;
    Mat r_hist2, b_hist2;
    Mat r_hist3, b_hist3;

    calcHist(&img_bgr1[0], 1, 0, Mat(), b_hist1, 1, &histSize, &histRange, uniform, accumulate);
    calcHist(&img_bgr1[2], 1, 0, Mat(), r_hist1, 1, &histSize, &histRange, uniform, accumulate);

    calcHist(&img_bgr2[0], 1, 0, Mat(), b_hist2, 1, &histSize, &histRange, uniform, accumulate);
    calcHist(&img_bgr2[2], 1, 0, Mat(), r_hist2, 1, &histSize, &histRange, uniform, accumulate);

    calcHist(&img_bgr3[0], 1, 0, Mat(), b_hist3, 1, &histSize, &histRange, uniform, accumulate);
    calcHist(&img_bgr3[2], 1, 0, Mat(), r_hist3, 1, &histSize, &histRange, uniform, accumulate);

    normalize(b_hist1, b_hist1, 0, b_hist1.rows, NORM_MINMAX, -1, Mat());
    normalize(b_hist2, b_hist2, 0, b_hist2.rows, NORM_MINMAX, -1, Mat());
    normalize(b_hist3, b_hist3, 0, b_hist3.rows, NORM_MINMAX, -1, Mat());
    normalize(r_hist1, r_hist1, 0, r_hist1.rows, NORM_MINMAX, -1, Mat());
    normalize(r_hist2, r_hist2, 0, r_hist2.rows, NORM_MINMAX, -1, Mat());
    normalize(r_hist3, r_hist3, 0, r_hist3.rows, NORM_MINMAX, -1, Mat());

    printf("I1 vs I2\nR:%f\nB:%f\n",
           compareHist(r_hist1, r_hist2, HISTCMP_CORREL),
           compareHist(b_hist1, b_hist2, HISTCMP_CORREL)
        );
    printf("I1 vs I3\nR:%f\nB:%f\n",
           compareHist(r_hist1, r_hist3, HISTCMP_CORREL),
           compareHist(b_hist1, b_hist3, HISTCMP_CORREL)
           );
}
