#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main(){

  Mat img = imread("drink-icon.png", IMREAD_COLOR);
  double red = 0;
  int n = img.rows * img.cols;

  for (int y = 0; y < img.rows; y++){
    for(int x=0; x < img.cols; x++){
      Vec3b color = img.at<Vec3b>(Point(x, y));
      red += color[2];
    }
  }

  printf("Total pixels: %d\n", n);
  printf("Red: %d",(int) (red/n) );

  return 0;
}
