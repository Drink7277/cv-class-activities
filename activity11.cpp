#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main() {
  VideoCapture cap("P6090053.avi");
  if (!cap.isOpened()) {
    return -1;
  }

  while(true) {
    Mat frame, edge;
    cap >> frame;

    if (frame.empty())
      break;

    cv::Canny(frame, edge, 20, 60, 3);
    imshow("Frame", frame );
    imshow("Edge", edge );

    char c = (char) waitKey(25); // esc
    if (c == 27)
      break;
  }

  cap.release();
  destroyAllWindows();
}
